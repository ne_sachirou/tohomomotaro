# encoding=utf-8

require 'rubygems'
require 'sinatra'

$PASSFRASES = [
  'v4ei', 'd219', 'qwy0', 'yotc', 'ba1z',
  'bfhe', '7nex', '6lot', 'tjcb', 'qns7',
  'xpsu', 'f1ox', 'xod1', 'iuiy', 'jifa',
  'x2nb', 'y573', '180w', 'uq13', 'a5yj',
  'jfm5', '5sfu', 'ppmy', 'bbgf', 'udkx',
  'utow', 'ibzv', 'yhv3', 'nwox', 'zhyp',
  'lhae', 'zj0y', '47fw', 'kamc', 'jn95',
  'kevg', '7uu4', '29op', 'ov2o', 'tc5e',
  '3b5f', 'e83p', 'yxr3', 'kch8', 'gfvy',
  '0ea8', 'wz34', 'zqri', '116z', '9ykq']


get '/' do
  send_file 'doc/index.html'
end


post '/auth' do
  passfrase = params[:passfrase]
  if $PASSFRASES.detect {|item| item == passfrase}
    redirect 'authed.html'
  else
    redirect "auth.html?passfrase=#{passfrase}"
  end
end


get '/favicon.png' do
  send_file 'favicon.png'
end


get %r{/(.+?)\.(html?|css|js|png|ogg|mp3)} do
  case params[:captures][1]
  when /html?/
    send_file "doc/#{params[:captures][0]}.html"
  when 'css'
    send_file "style/#{params[:captures][0]}.css"
  when 'js'
    send_file "script/#{params[:captures][0]}.js"
  else
    send_file "resource/#{params[:captures][0]}.#{params[:captures][1]}"
  end
end