// @description Define ES5 extention by ES3
// @author ne_Sachirou http://c4se.sakura.ne.jp/profile/ne.html
// @site https://gist.github.com/1018954
// @date 2011
// @license Public Domain

//java -jar compiler.jar --compilation_level SIMPLE_OPTIMIZATIONS --js_output_file es5.g.js --js es5.js

(function () {
'use strict';

var Array_prototype = Array.prototype;


if (!Array.isArray) {
    Array.isArray = function (obj) { // @param Object:
                                     // @return Boolean: obj is an Array or not
        return Object.prototype.toString.call(obj) === '[object Array]';
    };
}


if (!Array_prototype.every) {
    Array_prototype.every = function (fun,   // @param Function:
                                      obj) { // @param Object: this in fun
                                             // @return Boolean:
        var i = this.length - 1;

        for (; i >= 0; i -= 1) {
            if (typeof this[i] !== 'undefined' &&
                !fun.call(obj, this[i], i, this)) {
                return false;
            }
        }
        return true;
    };
}


if (!Array_prototype.filter) {
    Array_prototype.filter = function (fun,   // @param Function:
                                       obj) { // @param Object: this in fun
                                              // @return Array:
        var arr = [],
            i = 0,
            len = this.length;

        for (; i < len; i += 1) {
            if (typeof this[i] !== 'undefined' &&
                fun.call(obj, this[i], i, this)) {
                arr.push(this[i]);
            }
        }
        return arr;
    };
}


if (!Array_prototype.forEach) {
    Array_prototype.forEach = function (fun,   // @param Function:
                                        obj) { // @param Object: this in fun
                                               // @return Array: this
        var i = this.length - 1;

        for (; i >= 0; i -= 1) {
            if (typeof this[i] !== 'undefined') {
                fun.call(obj, this[i], i, this);
            }
        }
        return this;
    };
}


if (!Array_prototype.indexOf) {
    Array_prototype.indexOf = function (val,   // @param Object:
                                        num) { // @param Number=0:
                                               // @return Number: not found = -1
        var i,
            len = this.length;

        num = num || 0;
        while (num < 0) {
            num += len - 1;
        }
        for (i = num; i < len; i += 1) {
            if (this[i] === val) {
                return i;
            }
        }
        return -1;
    };
}


if (!Array_prototype.lastIndexOf) {
    Array_prototype.lastIndexOf = function (val,   // @param Object:
                                            num) { // @param Number=(this.length-1):
                                                   // @return Number: not found = -1
        var i,
            len = this.length;

        if (typeof num === 'undefined') {
            num = len - 1;
        }
        while (num < 0) {
            num += len - 1;
        }
        i = num;
        for (; i >= 0; i -= 1) {
            if (this[i] === val) {
                return i;
            }
        }
        return -1;
    };
}


if (!Array_prototype.map) {
    Array_prototype.map = function (fun,   // @param Function:
                                    obj) { // @param Object: this in fun
                                           // @return Array:
        var i = this.length,
            arr = new Array(i);

        for (; i >= 0; i -= 1) {
            if (typeof this[i] !== 'undefined') {
                arr[i] = fun.call(obj, this[i], i, this);
            }
        }
        return arr;
    };
}


if (!Array_prototype.reduce) {
    Array_prototype.reduce = function (fun,   // @param Function:
                                       val) { // @param Object:
                                              // @return Object:
        var i = 0,
            len = this.length;

        if (typeof val === 'undefined') {
            val = this[0];
            i = 1;
        }
        for (; i < len; i += 1) {
            if (typeof this[i] !== 'undefined') {
                val = fun.call(null, val, this[i], i, this);
            }
        }
        return val;
    };
}


if (!Array_prototype.reduceRight) {
    Array_prototype.reduceRight = function (fun,   // @param Function:
                                            val) { // @param Object:
                                                   // @return Object:
        var i = this.length - 1;

        if (typeof val !== 'undefined') {
            val = this[i];
            i -= 1;
        }
        for (; i >= 0; i -= 1) {
            if (typeof this[i] !== 'undefined') {
                val = fun.call(null, val, this[i], i, this);
            }
        }
        return val;
    };
}


if (!Array_prototype.some) {
    Array_prototype.some = function (fun,   // @param Function:
                                     obj) { // @param Object:
                                            // @return Boolean:
        var i = this.length - 1;

        for (; i >= 0; i -= 1) {
            if (typeof this[i] !== 'undefined' &&
                fun.call(obj, this[i], i, this)) {
                return true;
            }
        }
        return false;
    };
}


// http://blog.stevenlevithan.com/archives/faster-trim-javascript
if (!String.prototype.trim) {
    String.prototype.trim = function () { // @return String:
        var str = this.replace(/^\s\s*/, ''),
            ws = /\s/,
            i = str.length;

        while (ws.test(str.charAt(i -= 1))) {
        }
        return str.slice(0, i + 1);
    };
}


if (!Object.keys) {
    Object.keys = function (obj) { // @param Object:
                                   // @return Array[String]:
        var key, result = [];

        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                result.push(key);
            }
        }
        return result;
    };
}


if (!Date.now) {
    Date.now = function () {
        return new Date().getTime();
    };
}


if (!Date.prototype.toISOString) {
    Date.prototype.toISOString = function () {
        return (this.getUTCFullYear() < 1000 ?
                this.getUTCFullYear() < 100 ?
             this.getUTCFullYear() < 10 ?
                '000' :
                '00' :
                '0' :
                '') + this.getUTCFullYear() + '-' +
               (this.getUTCMonth() + 1 < 10 ? '0' : '') + (this.getUTCMonth() + 1) + '-' +
               (this.getUTCDate() < 10 ? '0' : '') + this.getUTCDate() + 'T' +
               (this.getUTCHours() < 10 ? '0' : '') + this.getUTCHours() + ':' +
               (this.getUTCMinutes() < 10 ? '0' : '') + this.getUTCMinutes() + ':' +
               (this.getUTCSeconds() < 10 ? '0' : '') + this.getUTCSeconds() + 'Z';
    };
}


// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Function/bind
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        var aArgs = Array.prototype.slice.call(arguments, 1), 
            fToBind = this, 
            fNOP = function () {};

        function fBound() {
            return fToBind.apply(this instanceof fNOP ? this : oThis || window,
                                 aArgs.concat(Array.prototype.slice.call(arguments)));
        };

        // closest thing possible to the ECMAScript 5 internal IsCallable function
        if (typeof this !== "function") {
            throw new TypeError("Function.prototype.bind - what is trying to be fBound is not callable");
        }

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();
        return fBound;
    };
}

}());