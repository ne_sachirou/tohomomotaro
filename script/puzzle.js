(function ($scope) {

var uu = $scope.uu,
    puzzle_state = [4, 2, 3, 1,
                    15, 12, 10, 0,
                    6, 11, 13, 5,
                    8, 14, 9, 7],
    right_puzzle_state = [1, 2, 3, 4,
                          5, 6, 7, 0,
                          8, 9, 10, 11,
                          12, 13, 14, 15];


function neighborhood(place) { // @param Number: pease place
                               // @return Array: Number[]
    var result = [];

    if (place % 4 !== 3) {
        result.push(place + 1);
    }
    if (place % 4 !== 0) {
        result.push(place - 1);
    }
    if (place <= 11) {
        result.push(place + 4);
    }
    if (place >= 4) {
        result.push(place - 4);
    }
    return result;
}


function has_empty_neighborhood(place) { // @param Number: pease place
                                         // @return Number: 0: false
                                         //                 1: left
                                         //                 2: down
                                         //                 3: up
                                         //                 4: right
    var neighbors = neighborhood(place).map(function (place) { // @param Number: peace place
                                                               // @return Number: peace number
        return puzzle_state[place];
    }),
        result,
        empty_place;

    if (neighbors.indexOf(0) === -1) {
        result = 0;
    } else {
        empty_place = puzzle_state.indexOf(0);
        switch (empty_place) {
        case place - 1:
            result = 1;
            break;
        case place - 4:
            result = 2;
            break;
        case place + 4:
            result = 3;
            break;
        case place + 1:
            result = 4;
            break;
        }
    }
    return result;
}


function point_of_place(place) { // @param Number: peace place
                                 // @return Array: Number[left, top]
    return [place % 4 * 100,
            place <= 3 ? 300 :
            place <= 7 ? 200:
            place <= 11 ? 100 : 0];
}


function is_right_place(peace_num) { // @param Number: peace number
                                     // @return Boolean:
    return puzzle_state.indexOf(peace_num) === right_puzzle_state.indexOf(peace_num);
}


function is_peaces_completed(){ // @return Boolean:
    var i = 0;

    for (; i < 16; i += 1) {
        if (puzzle_state[i] !== right_puzzle_state[i]) {
            return false;
        }
    }
    return true;
}


function move_peace(from_place) { // @param Number: peace place
    var to_place = neighborhood(from_place).filter(function (place) { // @param Number:
                                                                      // @return Boolean:
        return puzzle_state[place] === 0;
    }),
        to_point = point_of_place(to_place),
        target_node = uu.id('peace' + puzzle_state[from_place]);

    //uu.css(target_node, {left: to_point[0],
    //                     top: to_point[1]});
    uu.fx(target_node, 500, {left: [to_point[0], 'inoutquad'],
                              top: [to_point[1], 'inoutquad']});
    puzzle_state[to_place] = puzzle_state[from_place];
    puzzle_state[from_place] = 0;
    if (is_right_place(puzzle_state[to_place])) {
        uu.klass(target_node, '+rightpeace');
    } else {
        uu.klass(target_node, '-rightpeace');
    }
    if (is_peaces_completed()) {
        show_passed_message();
    }
}


function controll_puzzle_by_keybord(evt) { // @param Event: keydown event
    var keycode = evt.keyCode,
        i = 0;

    // h: 72, ��: 37
    // j: 74, ��: 40
    // k: 75, ��: 38
    // l: 76, ��: 39

    function move_peace_to_direction(direction) { // @param Number: 1: left
                                                  //                2: down
                                                  //                3: up
                                                  //                4: right
        for (; i < 16; i += 1) {
            if (has_empty_neighborhood(i) === direction) {
                move_peace(i);
                break;
            }
        }
    }

    if ([37, 38, 39, 40, 72, 74, 75, 76].indexOf(keycode) !== -1) {
        uu.event.stop(evt, true);
        if (keycode === 37 || keycode === 72) {
            move_peace_to_direction(1);
        } else if (keycode === 40 || keycode === 74) {
            move_peace_to_direction(2);
        } else if (keycode === 38 || keycode === 75) {
            move_peace_to_direction(3);
        } else if (keycode === 39 || keycode === 76) {
            move_peace_to_direction(4);
        }
    }
}


function attach_keyevent() {
    uu.event(document.body, 'keydown', controll_puzzle_by_keybord);
}


function detach_keyevent() {
    uu.event.unbind(document.body, 'keydown');
}


function show_passed_message() {
    detach_keyevent();
    uu.ajax('passed.html', {},
            function(response) {
        uu.tag('section')[1].innerHTML = response.rv;
    });
}
$scope.spm = show_passed_message;


function toggle_playaudio(target_node) { // @param Element:
    var target_audio_node = uu.tag('audio')[Number(target_node.parentNode.id.slice(5)) - 1],
        timerid;

    uu.klass('playaudio').
       filter(function (node) { // @param Element:
        return node !== target_node;
    }).
       forEach(function (node) { // @param Element:
        uu.klass(node, '+playwait');
        uu.klass(node, '-playing');
        node.src = 'playwait.png';
    });
    uu.tag('audio').
       filter(function (audio_node) { // @param Element:
        return audio_node !== target_audio_node;
    }).
       forEach(function (audio_node) { // @param Element:
        audio_node.pause();
    });

    if (uu.klass.has(target_node, 'playwait')) {
        uu.klass(target_node, '+playing');
        uu.klass(target_node, '-playwait');
        target_node.src = 'playing.png';
        target_audio_node.play();
        timerid = setInterval(function () {
            if (target_audio_node.currentTime <= 0 ||
                target_audio_node.currentTime >= target_audio_node.duration) {
                clearInterval(timerid);
                uu.klass(target_node, '+playwait');
                uu.klass(target_node, '-playing');
                target_node.src = 'playwait.png';
            }
        }, 100);
    } else {
        uu.klass(target_node, '+playwait');
        uu.klass(target_node, '-playing');
        target_node.src = 'playwait.png';
        target_audio_node.pause();
    }
}


uu.ready(function() {
    uu.klass('peace').forEach(function (node) { // @param Element:
        var peace_num = Number(node.id.slice(5));

        if (is_right_place(peace_num)) {
            uu.klass(node, '+rightpeace');
        }
        uu.click(node,
                 function (evt) { // @param Event: click event
            var peace_place = puzzle_state.indexOf(peace_num);

            if (has_empty_neighborhood(peace_place)) {
                move_peace(peace_place);
            }
        });
    });


    uu.klass('playaudio').forEach(function (node) { // @param Element:
        uu.event(node, 'mouseover', function (evt) { //@param Event:
            node.src = 'playhover.png';
        });

        uu.event(node, 'mouseout', function (evt) { //@param Event:
            if (uu.klass.has(node, 'playwait')) {
                node.src = 'playwait.png';
            } else {
                node.src = 'playing.png';
            }
        });

        uu.event(node, 'mousedown', function (evt) { //@param Event:
            node.src = 'playclick.png';
        });

        uu.event(node, 'click', function (evt) { //@param Event:
            uu.event.stop(evt, true);
            toggle_playaudio(node);
        });
    });


    uu.event(uu.id('toggle_keyevt'), 'change', function (evt) { // @param Event:
        if (uu.id('toggle_keyevt').checked) {
            attach_keyevent();
        } else {
            detach_keyevent();
        }
    });
});

}(this));
